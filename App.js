/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
} from 'react-native';
import LoginScreen from './src/screens/login/Login';
import AppointmentList from './src/screens/appointment/list/Appointmentlist';
import RegistrationScreen from './src/screens/login/MobileRegistration';
import MobileLogin from './src/screens/login/MobileLogin';
import VerificatonScreen from './src/screens/login/Verification'
import styles from './src/utils/Styles';
import userStyles from './src/css/userStyles'
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { Provider } from 'mobx-react';
import {
  Router,
  Scene,
  Actions,
  Modal
} from 'react-native-router-flux';
import DashboardScreen from './src/screens/home/dashboard';
import AppotintmentScreen from './src/screens/home/Appointment';
import ChatScreen from './src/screens/home/Chats';
import UserRoutes from './src/routes/UserRoutes';
import AsyncStorage from '@react-native-community/async-storage'
import NotificationStore from './src/appdata/NotificationStore';
import AppointmentListScreen from './src/screens/appointment/list/Appointmentlist';
import AppointmentDetailsScreen from './src/screens/appointment/AppointementDetails';
import { colors } from 'react-native-elements';
import IColors from './src/utils/Colors'

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      logged: false,
      dialogVisible: false,
      remotes: [],
      customerId: '',
    };
  }
  UNSAFE_componentWillMount() {
    AsyncStorage.getItem('access_token').then((value) => {
      this.setState({logged: value != null});
    });
  }
  render(){
    return(
      <Provider notificationStore = {NotificationStore} >
      <View style={Styles.container}>
        {this.state.logged !== "" ? (
          <Router
            navigationBarStyle={{ backgroundColor: IColors.primary }}
            titleStyle={{ color: "white", fontSize: 18, marginRight: 60 }}
          >
            <Modal hideNavBar>
              <Scene key="root">
                <Scene
                  component={AppointmentList}
                  hideNavBar
                  key="login"
                  initial={!this.state.logged}
                />
                <Scene 
                hideNavBar 
                component={RegistrationScreen} key="register" />
                <Scene 
                hideNavBar
                component={MobileLogin} 
                key="mLogin" />
                 <Scene 
                hideNavBar
                component={VerificatonScreen} 
                key="verification" />
                <Scene
                  component={DashboardScreen}
                  key="home"
                  initial={this.state.logged}
                />
                <Scene
                  component={AppointmentListScreen}
                  key="appointementList"
                />
                </Scene>
                <Scene
                  component={AppointmentDetailsScreen}
                  key="bookAppointement"
                  // hideNavBar
                />
                {/* </Scene> */}
               
            </Modal>
          </Router>
        ) : null}
      </View>
    </Provider>
    )
  }
 
  

}
