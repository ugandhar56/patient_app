import { decorate, observable, action } from "mobx";
import * as AppConstants from "../utils/AppConstants";
import NotifService from "../utils/NotifService";

class NotificationStore {
  // observable to count
  count = 0;

  // action to call API and search images
  getNotificationsCount = () => {
    fetch(
      AppConstants.BASE_URL +
        "user/" +
        global.customerId +
        "/notification/count",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          sessionId: global.sessionId,
        },
      }
    )
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status == AppConstants.STATUS_OK) {
          this.setCount(responseJson.data);
        } else if (responseJson.status == AppConstants.STATUS_INVALID) {
          Helpers.redirectToLogin();
        }
      });
  };

  // observables can be modifies by an action only
  setCount = (count) => {
    this.count = count;
  };

  // clears count & all notifications in tray
  clearCount = () => {
    this.count = 0;
    new NotifService().cancelAllNotifications();
  };
}

// another way to decorate variables with observable
decorate(NotificationStore, {
  count: observable,
  getNotificationsCount: action,
  setCount: action,
  clearCount: action,
});

// export class
export default new NotificationStore();
