import React from 'react';
import { SafeAreaView, View, FlatList,ImageBackground, StyleSheet, Text, StatusBar } from 'react-native';
import appoItemView from './AppointmentListItem'
import appointmentListStyle from './appointmentStyles'

const DATA = [
  {

    name: 'Dr.Niti Aggarawal',
    doctorSpecification:'MBBS Dr-General Physicion',
    experience:'5+ Years Experience',
    conseltationFee:'500',
    status:'Active',
    votes:'55 rating',
    rating:'4.5',
  },
  {
    
    name: 'Dr.Niti Aggarawal',
    doctorSpecification:'MBBS Dr-General Physicion',
    experience:'5+ Years Experience',
    conseltationFee:'500',
    status:'Closed',
    votes:'55 rating',
    rating:'4.5',
  },
  {
    
    name: 'Dr.Niti Aggarawal',
    doctorSpecification:'MBBS Dr-General Physicion',
    experience:'5+ Years Experience',
    conseltationFee:'500',
    status:'Active',
    votes:'55 rating',
    rating:'4.5',
  },
];



const AppointmentList = () => {


  return (
    
        <ImageBackground source={require('../../../../assets/img/login_bac.png')} style={appointmentListStyle.backgroundImage} >
      <FlatList
        data={DATA}
        renderItem={appoItemView}
        keyExtractor={item => item.id}
      />
      </ImageBackground>
    
  );
}

export default AppointmentList;
