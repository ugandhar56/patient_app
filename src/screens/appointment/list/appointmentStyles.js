
import { StyleSheet, StatusBar } from 'react-native';
const appointmentListStyle = StyleSheet.create(
    {

        ////////////////////////  list view styles ///////////////////////////
        container:
        {
            flex: 1,
            marginTop: StatusBar.currentHeight || 0,
        },
        backgroundImage: {
            flex: 1,
            width: null,
            height: null,
        },
        /////////////////////// item styles ///////////////////////////
        itemCard: {
            padding: 5,
            margin: 5 ,
            width:null
        },
        topColunmContainer:
        {
            flexDirection: 'column',
        },
        topColunmContainer:
        {
            flexDirection: 'column',
        },
        topRowContainer:
        {
            flexDirection: 'row',
            alignItems:'center',
            paddingBottom:10,
        
            
        },
        topSubRowContainer:
        {
            flexDirection: 'row',
            alignItems:'center',
            
        },
        middleColumnStyle:{
            flexDirection:'column',
            padding:5,
            flex:0.87
        },
        bottomRowContainer:
        {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignContent:'center',
            padding:10,
            
            
        },
        doctorNameStyle:{
            color:'#216ECB',
            margin: 5,
            fontSize: 16,
        },
        doctorSpecificationStyle:{
            color:'grey',
            margin: 5,
            fontSize: 13,
        },
        experienceStyle:{
            color:'#0ab75b',
            margin: 5,
            fontSize: 14,
            paddingLeft:5,
            paddingRight:5,
            backgroundColor:'#a9fdd0',
            alignSelf:'baseline'
        },
        ratingContainer:{
            flexDirection:'column',
            alignItems:'flex-end',
            padding:5,
            alignSelf:'flex-start'
        },
        ratingStyle:{
                backgroundColor:'orange',
                color:'white',
                paddingLeft:5,
                paddingRight:5,
                
        },
        ratingVotesStyle:{

        },
        dividerStyle:{
            borderBottomColor: 'black',
            borderBottomWidth: 0.5,
            width:null
          },
          consultationContainer:{
            flexDirection: 'row',
            alignContent:'center',
          },
          consultationFee1:{
                color:'grey',
                fontSize:14,
                alignSelf:'center'
          },
          consultationFee2:{
            color:'black',
            fontSize:14,
            fontWeight:'bold',
            alignSelf:'center',
          },
          activeBookContainer:{
            flexDirection:'row',
            
            
          },

          activeStyle:{
            color:'green',
        
            padding:5,
            fontSize:14,
            alignSelf:'center'
          },
          cancelStyle:{
            padding:5,
            color:'red',
            fontSize:14,
            alignSelf:'center'
          },
          bookNowActiveStyle:
          {
            
            paddingLeft:7,
            paddingRight:7,
            padding:5,
            color:'white',
            fontSize:14,
            alignSelf:'center',
            backgroundColor:'#216ECB',
            borderRadius:7,
          },
          bookNowDisableStyle:
          {
            
            paddingLeft:7,
            paddingRight:7,
            padding:5,
            color:'white',
            fontSize:14,
            alignSelf:'center',
            backgroundColor:'#CECECE',
            borderRadius:7,
          }


    }
);

export default appointmentListStyle;