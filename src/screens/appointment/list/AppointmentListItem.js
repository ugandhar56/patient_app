import React from 'react';
import { View, Text, StyleSheet, Image, Button, TouchableOpacity } from 'react-native';
import AppointmentListDTO from './AppointmentListDTO';
import { Card } from 'react-native-shadow-cards';
import appointmentListStyle from './appointmentStyles';
import { Avatar } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';

const appoItemView = ({ item }) => (

    <Card style={appointmentListStyle.itemCard}>
        <View style={appointmentListStyle.topColunmContainer}>
            <View style={appointmentListStyle.topRowContainer}>
                <Avatar
                    rounded
                    size="large"
                    source={{
                        uri:
                            'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                    }}
                />
            
                <View style={appointmentListStyle.topSubRowContainer}>
                    <View style={appointmentListStyle.middleColumnStyle}>
                        <Text style={appointmentListStyle.doctorNameStyle}>
                            {item.name}
                        </Text>
                        <Text style={appointmentListStyle.doctorSpecificationStyle}>
                            {item.doctorSpecification}
                        </Text>
                        <Text style={appointmentListStyle.experienceStyle}>
                            {item.experience}
                        </Text>

                    </View>

                    <View style={appointmentListStyle.ratingContainer}>
                        <Text style={appointmentListStyle.ratingStyle}>{item.rating}</Text>
                        <Text style={appointmentListStyle.ratingVotesStyle}>{item.votes}</Text>
                    </View>
                </View>
            </View>
            <View style={appointmentListStyle.dividerStyle} />
            <View style={appointmentListStyle.bottomRowContainer}>
                <View style={appointmentListStyle.consultationContainer}>
                    <Text style={appointmentListStyle.consultationFee1}>Consultation Fee </Text>
                    <Text style={appointmentListStyle.consultationFee2}>{item.conseltationFee}</Text>
                </View>
                <View style={appointmentListStyle.activeBookContainer}>
                    <Text style={item.status == "Active" ?
                        appointmentListStyle.activeStyle :
                        appointmentListStyle.cancelStyle}>{item.status}</Text>
                    <TouchableOpacity
                    onPress ={() => {
                    Actions.push("bookAppointement");
              }}>
                        <Text style={item.status == "Active" ?appointmentListStyle.bookNowActiveStyle:appointmentListStyle.bookNowDisableStyle}>
                            Book Now
                         </Text>
                         
                    </TouchableOpacity>

                </View>
            </View>
        </View>
    </Card>

);
export default appoItemView;