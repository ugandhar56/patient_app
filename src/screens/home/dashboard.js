import React,{ Component } from "react";
import { View } from "react-native-animatable";
import {Text,Modal,TouchableOpacity,Dimensions,ScrollView,
  TextInput,Image,StyleSheet} from 'react-native';
import En from '../../utils/En';
import Colors from '../../utils/Colors.js';
import Menu from "../../components/Menu";
import Icon from "react-native-vector-icons/MaterialIcons";
import HeaderRightButtons from "../../components/HeaderRightButtons";
import SideMenu from "react-native-side-menu";
import LoadingIndicator from "../../components/LoadingIndicator.js";
import { inject, observer } from "mobx-react";
import { Actions } from "react-native-router-flux";
const menuServicesData = [
    {
      name: En.medical_history,
      image: require("../../../assets/images/medical_history.png"),
      screen: "medicalHistory",
    },
    {
      name: En.family_members,
      image: require("../../../assets/images/family_members.png"),
      screen: "familyMembers",
    },
    {
      name: En.manage_address,
      image: require("../../../assets/images/manage_address.png"),
      screen: "manageAddress",
      params: { type: "manage" },
    },
    {
      name: En.change_password,
      image: require("../../../assets/images/password.png"),
      screen: "changePassword",
    },
    {
      name: En.menu_contact_us,
      image: require("../../../assets/images/menu_contact_us.png"),
      screen: "contactUs",
    },
  ];

  let windowHeight = Dimensions.get("window").height;

class DashboardScreen extends Component
{
    static navigationOptions  = {
        
          title: En.app_name,
          color:Colors.primary,
          
        
      };
    
      constructor(props){
        super(props);
        this.state = {
           isOpen : false,
           firstName:'Yugandhar',
           lastName:'Jututru',
           profileImage:null,
           instantBookModalVisible:false
        }
      }
      updateMenuState(isOpen) {
        this.setState({ isOpen });
      }
      onMenuItemSelected = (item) => {
        item.screen != ""
          ? this.redirectToAnotherScreen(item.screen, item.params)
          : Toast.show("Coming Soon", Toast.LONG);
        this.setState({
          isOpen: false,
          selectedItem: item,
        });
      };
      redirectToAnotherScreen() {
        Actions.push("appointementList");
      }
    render(){
        const menu = (
            <Menu
              onItemSelected={this.onMenuItemSelected}
              firstName={"yugandhar"}
              lastName={"juturu"}
              data={menuServicesData}
              profilePic={null}
              profileClick={() => {
                // this.redirectToAnotherScreen("profile");
                this.setState({
                  isOpen: false,
                });
              }}
              logoutClick={() => {
                  console.log("==== logout pressed======")
                // this.logout();
                this.setState({
                  isOpen: false,
                });
              }}
            />
          );
      
        return(
            <SideMenu
            menu={menu}
            isOpen={this.state.isOpen}
            onChange={(isOpen) => this.updateMenuState(isOpen)}
          >
    
    <View style={Styles.container}>
         {this.mobileDesign()}

          {this.state.isLoading ? <LoadingIndicator /> : null}

          <Modal
            transparent={true}
            visible={this.state.instantBookModalVisible}
          >
            <View style={styles.modalDialogModalView}>
              <View style={styles.instantModalDialogView}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      instantBookModalVisible: !this.state
                        .instantBookModalVisible,
                    });
                  }}
                  style={styles.cancelModalTouchView}
                >
                  <Icon name="clear" size={30} color="red" />
                </TouchableOpacity>
                <View>
                  <Text style={styles.icText}>
                    {En.instant_consultation_description}
                  </Text>

                  <View style={styles.complaintsTextInputView}>
                    <TextInput
                      style={styles.complaintsTextInput}
                      placeholder={En.complaints}
                      placeholderTextColor={Colors.text_input_placeholder}
                      onChangeText={(textinputComplaints) =>
                        this.setState({
                          textinputComplaints,
                        })
                      }
                      underlineColorAndroid="transparent"
                      multiline={true}
                    />
                  </View>

                  <Text
                    onPress={() => {
                      this.redirectToInstantConsultBooking();
                    }}
                    style={styles.bookButton}
                  >
                    {En.book}
                  </Text>
                </View>
              </View>
            </View>
          </Modal>
          {/* <PatientFeedback
            ref={(ref) => (this.feedbackDialog = ref)}
            showSkipButton={true}
          /> */}
        </View>

     </SideMenu>
        )
    }

    mobileDesign() {
      return (
        <ScrollView style={styles.mobileScrollView}>
          <View>
          {this.greetAndWelcomeTextDesign()}
          <View style={styles.dashboardItemsView}>
          <TouchableOpacity
            style={styles.dashboardItemTouchView}
            onPress={this.openInstantConsultAlert}
          >
            <Image
              source={require("../../../assets/images/instant_consultation.png")}
              style={styles.dashboardItemImage}
            />
            <View style={styles.dashboardItemView}>
              <Text style={styles.dashboardItemText}>
                {En.dashboard_instant_consultation}
              </Text>
              <Text style={styles.dashboardItemSubText}>{En.call_now}</Text>
            </View>
          </TouchableOpacity>

         
        </View>
        <TouchableOpacity
          style={styles.dashboardItemTouchView}
            onPress={this.redirectToAnotherScreen}>
                <View style={styles.dashboardItemView}>
              <Text style={styles.dashboardItemText}>
                {En.book_appointment}
              </Text>
            </View>
          </TouchableOpacity>

          {/* {this.commonDesign()} */}
           
          </View>
        </ScrollView>
      );
    }
    greetAndWelcomeTextDesign() {
      return (
        <View style={{ margin: 10 }}>
          <Text style={styles.greetText}>
            {En.hello} {this.state.firstName}
          </Text>
          <Text style={styles.welcomeText}>
            {En.welcome_to} {En.app_name}
          </Text>
        </View>
      );
    }
    commonDesign() {
      return (
        <View>
          {/* {this.servicesDesign()} */}
          <Text style={styles.popularServicesFieldText}>
            {En.popular_services}
          </Text>
          {/* <FlatList
            style={styles.popularServicesList}
            data={this.state.popularServicesList}
            renderItem={({ item, index }) => this.servicesRenderRow(item, index)}
            horizontal={true}
            extraData={this.state.popularServiceTextHeight}
          />
   */}
          {/* <Text style={styles.moduleText}>{En.packages}</Text>
          <FlatList
            style={styles.packageList}
            data={this.state.packageList}
            renderItem={({ item }) => this.packagesRenderRow(item)}
            horizontal={true}
            extraData={this.state.packageTextHeight}
          /> */}
  
          {/* <Text style={styles.healthTipFieldText}>{En.health_tips}</Text> */}
          {/* <FlatList
            style={styles.healthTipList}
            data={this.state.healthTipsImagesList}
            renderItem={({ item }) => this.tipsRenderRow(item)}
            horizontal={true}
          /> */}
        </View>
      );
    }

}
const styles = StyleSheet.create({
  mobileScrollView: {
    flex: 1,
  },
  tabletView: {
    flex: 1,
    justifyContent: "flex-end",
  },
  tabletCarouselView: {
    flex: 1,
  },
  healthTipFieldText: {
    marginTop: 10,
    marginBottom: 5,
    color: Colors.headerText,
    fontSize: 16,
    fontFamily: "OpenSans-Bold",
    marginLeft: 10,
  },
  healthTipImage: {
    marginRight: 5,
    marginLeft: 5,
  },
  healthTipList: {
    marginTop: 5,
    marginBottom: 10,
    marginRight: 5,
    marginLeft: 5,
  },
  popularServicesFieldText: {
    marginTop: 20,
    marginBottom: 5,
    color: Colors.headerText,
    fontSize: 16,
    fontFamily: "OpenSans-Bold",
    marginLeft: 10,
  },
  popularServicesList: {
    marginRight: 5,
    marginLeft: 5,
  },
  popularServiceItemTouchView: {
    width: 150,
    borderRadius: 10,
    margin: 5,
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
  },
  popularServiceItemServiceText: {
    color: Colors.headerText,
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
  },
  popularServiceItemCostText: {
    marginTop: 5,
    color: Colors.subText,
    fontSize: 14,
    fontFamily: "OpenSans-Bold",
    textAlign: "center",
  },
  popularServiceItemImage: {
    width: 36,
    height: 36,
    marginBottom: 5,
    alignSelf: "center",
  },
  packageList: {
    marginRight: 5,
    marginLeft: 5,
  },
  packageItemServiceText: {
    color: "black",
    fontSize: 14,
    fontWeight: "bold",
    width: 114,
  },
  packageItemCostText: {
    color: "dimgrey",
    marginTop: 5,
    fontSize: 14,
  },
  packageItemTouchView: {
    backgroundColor: "white",
    borderRadius: 10,
    margin: 5,
    padding: 10,
  },
  packageItemView: {
    flexDirection: "row",
  },
  packageItemImage: {
    width: 58,
    height: 58,
    borderRadius: 5,
    marginRight: 10,
  },
  modalDialogModalView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  cancelModalTouchView: {
    alignSelf: "flex-end",
    margin: 5,
  },
  promotionsImage: {
    height: 150,
    width: Dimensions.get("window").width,
  },
  bookButton: {
    color: "white",
    alignSelf: "center",
    backgroundColor: Colors.primary_dark,
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 50,
    paddingLeft: 50,
    borderRadius: 10,
    margin: 20,
  },
  icText: {
    margin: 10,
    color: "black",
    fontSize: 18,
    textAlign: "center",
  },
  instantModalDialogView: {
    backgroundColor: "white",
    borderRadius: 10,
    width: Dimensions.get("window").width - 50,
  },
  dashboardItemsView: {
    flexDirection: "row",
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 10,
  },
  dashboardItemTouchView: {
    flex: 1,
    flexDirection: "row",
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 1.0,
    shadowRadius: 10.0,
    elevation: 20,
    padding: 10,
  },
  dashboardItemView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  dashboardItemText: {
    color: Colors.headerText,
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Montserrat-Bold",
  },
  dashboardItemSubText: {
    marginTop: 5,
    color: Colors.subText,
    fontSize: 14,
    textAlign: "center",
    fontFamily: "OpenSans-Bold",
  },
  dashboardItemImage: {
    width: 36,
    height: 36,
    alignSelf: "center",
    marginRight: 5,
  },
  divider: {
    borderBottomColor: "gray",
    borderBottomWidth: 1,
    width: "100%",
    marginTop: 10,
    marginBottom: 5,
  },
  complaintsTextInputView: {
    flexDirection: "row",
    height: 75,
    margin: 10,
  },
  complaintsTextInput: {
    flex: 1,
    fontSize: 16,
    color: Colors.text_input,
    borderWidth: 1,
    borderColor: Colors.text_input_placeholder,
    borderRadius: 10,
  },
  greetText: {
    color: Colors.greetText,
    fontSize: 18,
    fontFamily: "OpenSans-Bold",
    alignSelf: "center",
  },
  welcomeText: {
    marginTop: 5,
    // color: Colors.subText,notificationStore,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    alignSelf: "center",
  },
});

export default inject("notificationStore")(observer(DashboardScreen));