import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  TextInput,
  Animated,
  Text,
  StyleSheet,
  ActivityIndicator,
  Easing,
  Dimensions,
  Image,
  ImageBackground,
  TouchableHighlight
} from 'react-native';
import { Actions} from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage'
// import {decode as atoa, encode as btoa} from 'base-64';
import Icon from 'react-native-vector-icons/MaterialIcons';
import App from '../../../App';
import Styles from '../../utils/Styles';
import * as Animatable from 'react-native-animatable';
import En from '../../utils/En';
import Colors from '../../utils/Colors.js';
import CheckBox from '../../components/CheckBox';
import {
  serverConnection
} from '../../services/Connect';
import IConstants from '../../config/Constants';
import {
  handleAndroidBackButton,
  removeAndroidBackButtonHandler,
} from '../../components/AndroidBackHandling';
import ShowError from '../../components/ShowError';
import Helpers from '../../utils/Helpers';
// import SocketManager from '../components/SocketManager';
import * as AppConstants from '../../utils/AppConstants';
import icoMoonConfig from '../../../selection.json';
import {BaseURL,METHOD_LOGIN} from '../../config/serviceURLS';
import {decode as atob, encode as btoa} from 'base-64'
import { connect } from 'socket.io-client';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
const SvgIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

export default class Login extends Component {
  constructor(props) {
    super(props)
    console.log("entered into Login ====")
    this.state = {
      isLoading: false,
      hidePassword: true,
      textInputEmail: '',
      textInputPassword: '',
      // colorAnim: new Animated.Text('#000'),
      FirstName: '',
      LastName: '',
      rememberme: false,
    };
    this.loginUser = this.loginUser.bind(this);
    this.onClickListener = this.onClickListener.bind(this);

    //setting login button animation to zero
    this.loginButtonAnimated = new Animated.Value(0);
    //setting color animation to zero
    this.colorAnimated = new Animated.Value(0);
  }

  componentWillUnmount() {
    // removeAndroidBackButtonHandler();
    // this.didFocusListner.remove();
  }

  onClickListener = (viewId) => {
    // alert( "Button pressed " + viewId);
      Actions.push("register");
    // }
  }
  
  //showing full color screen when routing to next screen
  showFullColorScreen() {
    Animated.timing(this.colorAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();
  }

  loginUser = () => 
  {
   
    // this.moveToMainAppScreen("123")
  
    // restricting user for double click
    if (this.state.isLoading) {
      return;
    }

    const {
      textInputEmail,
      textInputPassword
    } = this.state;
    if (textInputEmail.trim() === '' && textInputPassword.trim() === '') {
      Toast.show('Please enter username and password', Toast.LONG);
    } else if (textInputEmail.trim() == '') {
      Toast.show('Please enter username', Toast.LONG);
    } else if (
      isNaN(textInputEmail.trim()) &&
      !Helpers.validateEmail(textInputEmail.trim())
    ) {
      Toast.show('Please enter valid Email Id', Toast.LONG);
    } else if (
      !isNaN(textInputEmail.trim()) &&
      textInputEmail.trim().length !== 10
    ) {
      Toast.show('Mobile number should be 10 digits', Toast.LONG);
    } else if (textInputPassword.trim() == '') {
      Toast.show('Please enter password', Toast.LONG);
    } else if (textInputPassword.trim().length < 6) {
      Toast.show('Password should be at least 6 characters', Toast.LONG);
    } else if (textInputPassword.trim().length > 15) {
      Toast.show('Password should not exceed 15 characters', Toast.LONG);
    } else if (!Helpers.validateSpaceInPassword(textInputPassword.trim())) {
      Toast.show(En.error_password_space, Toast.LONG);
    } 
    else {
      this.onFetchLoginRecords();
    }
  };

  moveToMainAppScreen(sessionId) {
    console.log("====== next screen =====")
    Actions.push("home",{session:sessionId});
  }
  // checkPermission() {
  //   Permissions.request('camera').then((cameraPermission) => {
  //     if (cameraPermission == 'authorized') {
  //       Permissions.request('microphone').then((microphonePermission) => {
  //         if (microphonePermission == 'authorized') {
  //           AsyncStorage.getItem('fcmToken').then((fcmToken) => {
  //             this.onFetchLoginRecords(fcmToken);
  //           });
  //         } else if (microphonePermission === 'restricted') {
  //           alert(En.video_call_login_microphone_permission_restricted);
  //         } else {
  //           alert(En.video_call_permission_denied_login);
  //         }
  //       });
  //       // @ts-ignore
  //     } else if (cameraPermission === 'restricted') {
  //       alert(En.video_call_login_camera_permission_restricted);
  //     } else {
  //       alert(En.video_call_permission_denied_login);
  //     }
  //   });
  // }

  onFetchLoginRecords(fcmToken) {
    this.setState({
      isLoading: true
    });
    Animated.timing(this.loginButtonAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();
    let formdata = new FormData();
    formdata.append("username",this.state.textInputEmail.trim())
    formdata.append("password",this.state.textInputPassword.trim())
    formdata.append("grant_type",'password')
   
    const URL = BaseURL+"/"+METHOD_LOGIN;

    console.log("==== URL" +URL );
    serverConnection.sendLoginRequest(formdata,this.loginHandler,this.errorHandler)

  }
  loginHandler = (iStatus, sMessage, reposnceData) => {
    this.setState({
      isLoading: false
    });
    if (iStatus > 0) {
            if (reposnceData.customer) {
            AsyncStorage.setItem("access_token", JSON.stringify(reposnceData.access_token));
            AsyncStorage.setItem("refresh_token", JSON.stringify(reposnceData.refresh_token));
            AsyncStorage.setItem(
              "rememberme",
              this.state.rememberme.toString()
            );
            AsyncStorage.setItem("password",btoa(this.state.textInputPassword.trim()));
            AsyncStorage.setItem("username",this.state.textInputEmail.trim());
            AsyncStorage.setItem("logined", true);
            // AsyncStorage.setItem("username",this.state.textInputEmail.trim());
            

            // this.loginButtonAnimated.setValue(0);
            this.moveToMainAppScreen(reposnceData.access_token);
          } else {
            this.loginButtonAnimated.setValue(0);
            Toast.show("Please login using a valid credentials", Toast.LONG);
          }
        } else {
          Toast.show(
            "Authentication failed due to invalid username or password!!!",
            Toast.LONG
          );
          this.setState({ isLoading: false });
      // const data = { iUserId:reposnceData.iUserId,sUserName:reposnceData.sUser,sSessionId:reposnceData.sSession }
      // this.props.saveAppData(data);
      // Actions.push("home");
    }
  };

  errorHandler = (iStatus) => {
    this.setState({
      isLoading: false
    });
  };

  managePasswordVisibility = () => {
    this.setState({
      hidePassword: !this.state.hidePassword
    });
  };

  handleRememberMe = () => {
    console.log("remember ========" +this.state.rememberme)
    this.setState({
      rememberme: !this.state.rememberme
    });
  };

  componentDidMount(){
    console.log("======== did mount ========")
  }
  openTermsAndConditionsLink = () => {
    Actions.push('webview', {
      url: AppConstants.TERMS_AND_CONDITIONS_URL
    });
  };

  render() {
    // let keyType = 'done';
    // if (this.props.special) {
    //   keyType = 'next';
    // }
    // const changeWidth = this.loginButtonAnimated.interpolate({
    //   inputRange: [0, 1],
    //   outputRange: [DEVICE_WIDTH - 60, 30],
    // });
    // const changeScale = this.colorAnimated.interpolate({
    //   inputRange: [0, 1],
    //   outputRange: [1, 30],
    // });
    return ( 
      <ImageBackground 
      source={require('../../../assets/img/login_bac.png')} 
      style={styles.backgroundImage} >

      <View 
      style={styles.container}
      resizeMode="stretch">

          <View >
              <Image style={styles.iConStyle}

                  source={require('../../../assets/img/logo.png')}
                  resizeMode="contain"
              />
          </View>

          <View>
              <Text style={{ marginTop: 5, margin: 10, fontSize: 20, }}> Welcome to Med-360</Text>
          </View>

          <View style={styles.inputContainer}>
          <SvgIcon
          style={styles.inputIcon}
          name='user'
          size={18}
          color='gray'
        />
              <TextInput style={styles.inputs}
                  placeholder={En.mobile}
                  keyboardType="number-pad"
                  underlineColorAndroid='transparent'
                  value ={this.state.textInputEmail}
                  onChangeText={(textInputEmail) => this.setState({ textInputEmail })} />
          </View>

          <View style={styles.inputContainer}>
          <SvgIcon
          style={styles.inputIcon}
          name='password'
          size={18}
          color='gray'
        />
              <TextInput style={styles.inputs}
                  placeholder={En.password}
                  secureTextEntry={true}
                  underlineColorAndroid='transparent'

                  onChangeText={(textInputPassword) => this.setState({ textInputPassword })} />
          </View>

          <View style={styles.checkboxStyle}>
              <CheckBox
                 selected={this.state.rememberme}
                onPress={this.handleRememberMe}
                text={En.remember_me}
                textStyle={{ color: "blue" }}
              />
              {/* <Text style={{ marginTop: 4, color: 'white', fontSize: 17 }}> {En.remember_me}</Text> */}
          </View>

          <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={ this.loginUser}>
          {this.state.isLoading ? (
                  <ActivityIndicator size="small" color="white" />
                ) : (
                  <Text style={styles.loginText}>{En.login}</Text>
                )}
              
          </TouchableHighlight>

          <TouchableHighlight style={styles.buttonContainer} >
              <Text style={styles.forgetPasswordStyle}>{En.forgot_password}</Text>
          </TouchableHighlight>
          {/* onPress={ this.onClickListener('restore_password')} */}

          <View style={styles.signUpContainerStyle}>
              <Text style={styles.signUpStyle}> {En.do_you_have_an_account}</Text>
              <TouchableHighlight  onPress={this.onClickListener}>
                  <Text style={styles.signUpStyle2}>{En.sign_up}</Text>
              </TouchableHighlight>
              {/* onPress={this.onClickListener('signUp')} */}
          </View>
      </View>
  </ImageBackground>
    );
  }
}

// Login.propTypes = {
//   update: PropTypes.func.isRequired,
//   changeFocus: PropTypes.func.isRequired,
//   special: PropTypes.bool,
// };

Login.defaultProps = {
  special: false,
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      // backgroundColor: '#DCDCDC',

  },
  backgroundImage: {
      flex: 1,
      width: null,
      height: null,

  },
  imageContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#000000',
      width: 30,
      height: 30,
      alignItems: 'center',
      flexDirection: 'column'
  },

  inputContainer: {
  
      borderColor:'blue',
      backgroundColor: '#FFFFFF',
      borderRadius: 25,
      borderWidth: 1,
      //   width:250,
      height: 55,
      marginBottom: 20,
      marginLeft: 20,
      marginRight: 20,
      
      flexDirection: 'row',
      alignItems: 'center'
  },
  inputs: {
      height: 55,
      marginLeft: 16,
      borderBottomColor: '#FFFFFF',
      flex: 1,
  },
  iConStyle: {
      marginLeft: 16,
      borderBottomColor: '#FFFFFF',
      margin: 10,
      width: 150,
      height: 85


  },
  inputIcon: {
      width: 30,
      height: 30,
      marginLeft: 20,
      marginTop:10,
      justifyContent: 'center'
  },
  checkboxStyle: {
      flexDirection: 'row',
      alignSelf: 'stretch',
      justifyContent: 'flex-start',
      marginLeft: 20,
      alignItems: 'flex-start',
  },
  buttonContainer: {
      height: 55,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 20,
      // width:250,
      alignSelf: 'stretch',
      borderRadius: 25,
  },
  forgetPasswordStyle: {
      textAlign: 'center',
      color: '#171aea',
      textDecorationLine: 'underline',
      fontSize: 18

  },
  signUpContainerStyle: {
      flexDirection: 'row',
      width: '100%',
      height:45,
      backgroundColor:'#acb0b4',
      justifyContent: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      position: 'absolute', //Here is the trick
      fontSize: 18,
      bottom: 0,
  },
  signUpStyle: {
      color: 'black',
  },
  signUpStyle2: {
      
      color: 'red',    
      margin:5,
      textDecorationLine: 'underline',

  },
  loginButton: {
      backgroundColor: "#f06a2f",
  },
  loginText: {
      color: 'white',
      fontSize: 18
  }
});