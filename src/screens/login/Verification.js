import React, { Component } from 'react';
import Toast from 'react-native-simple-toast';

import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    ImageBackground,
    Alert,
    ActivityIndicator,
    CheckBox,
    ToastAndroid
} from 'react-native';
import En from '../../utils/En';
import AsyncStorage from '@react-native-community/async-storage'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../../selection.json';
const SvgIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
import OtpInputs from "react-native-otp-inputs";
import Colors from '../../utils/Colors'
import RNOtpVerify from 'react-native-otp-verify';
import { serverConnection } from '../../services/Connect';
import VerificationStyle from '../../css/login/VerificationStyle'
import { Actions} from 'react-native-router-flux';

// import { Colors } from 'react-native/Libraries/NewAppScreen';

export default class Verification extends Component {

    constructor(props) {
        super(props)
        this.state = {
            otp: "",
            mobile: this.props.mobile,
            newPassword: "",
            confirmPassword: "",
            isLoading: false,
            // otpCode:''
        }
    }

    componentWillMount() {
        this.getHash();
        this.startListeningForOtp();
    }

    onClickListener = (viewId) => {
        // this.setState({
        //     mobile: this.props.mobile
        // })
        if (this.state.isLoading) {
            return;
          }
        const {
            newPassword,
            confirmPassword,
            otp
          } = this.state;
        if (this.state.otp.trim()=='')
            Toast.show(En.enter_otp, Toast.LONG);
        if (this.state.newPassword.trim() =='')
            Toast.show(En.error_password, Toast.LONG);
        else if(this.state.newPassword.trim().length<8 )
            Toast.show(En.password_min_chars, Toast.LONG);
        else if (!(/^([a-zA-Z0-9]+)$/.test(newPassword) && /\d/.test(newPassword) && /[A-Z]/i.test(newPassword)))
            Toast.show(En.password_contain, Toast.LONG);
        else if (this.state.confirmPassword.trim()== '')
            Toast.show(En.error_confirm_password, Toast.LONG);
        else if (this.state.confirmPassword.trim()!= this.state.newPassword.trim())
            Toast.show(En.invalid_password_confirm_password, Toast.LONG);
        else {

            // let url = "http://35.184.203.244/patient/api/otp/" + this.props.mobile + "/" + this.state.otpCode;
            // serverConnection.sendPostRequest(url, this.handleResponce)
        
            console.log("=== otp ======="+ this.state.otp.trim());
            console.log("=== newPassword ======="+ this.state.newPassword.trim());
            console.log("=== sMobile ======="+ this.state.mobile.trim());
            

            const verifyObject = {
                mobile: this.state.mobile.trim(),
                otp: this.state.otp.trim(),
                password: this.state.newPassword.trim()
              };
              
              const sVerifyJson = JSON.stringify(verifyObject);
              this.setState({
                isLoading: true
              }); 
            serverConnection.verifyOtpWithPWD(sVerifyJson, this.handleResponce,this.errorHandler)

        }

    }
    getHash = () =>
        RNOtpVerify.getHash()
            .then(console.log)
            .catch(console.log);

    startListeningForOtp = () =>
        RNOtpVerify.getOtp()
            .then(p => RNOtpVerify.addListener(this.otpHandler))
            .catch(p => console.log(p));

    otpHandler = (message) => {
        console.log("recive message ==")
        console.log("message =========" + (message))
        // const otpCode = /(\d{4})/g.exec(message)[1];
        // this.setState({ otpCode });
        RNOtpVerify.removeListener();
        // Keyboard.dismiss();
    }
    handleResponce = (reposnceData) => {

        console.log("handle response =====" + JSON.stringify(reposnceData));
        let formdata = new FormData();
        formdata.append("username",this.state.mobile)
    formdata.append("password",this.state.newPassword)
    formdata.append("grant_type",'password')
        serverConnection.sendLoginRequest(formdata,this.loginHandler,this.errorHandler)
        // Actions.push("dashboard")
    }
    loginHandler = (iStatus, sMessage, reposnceData) => {
        this.setState({
          isLoading: false
        });
        if (iStatus > 0) {
                if (reposnceData.customer) {
                AsyncStorage.setItem("access_token", JSON.stringify(reposnceData.access_token));
                AsyncStorage.setItem("refresh_token", JSON.stringify(reposnceData.refresh_token));
                AsyncStorage.setItem("logined", true);
                Actions.push("home",{back:false});
              } else {
                this.loginButtonAnimated.setValue(0);
                Toast.show("Please login using a valid credentials", Toast.LONG);
              }
            } else {
              Toast.show(
                "Authentication failed due to invalid username or password!!!",
                Toast.LONG
              );
              this.setState({ isLoading: false });
          // const data = { iUserId:reposnceData.iUserId,sUserName:reposnceData.sUser,sSessionId:reposnceData.sSession }
          // this.props.saveAppData(data);
          // Actions.push("home");
        }
      };
      errorHandler = (iStatus) => {
        this.setState({
          isLoading: false
        });
      };
    render() {
        return (
            <ImageBackground source={require('../../../assets/img/login_bac2.png')} style={VerificationStyle.backgroundImage} >
                <View style={VerificationStyle.container}>
                    <View >
                        <Image style={VerificationStyle.iConStyle}

                            source={require('../../../assets/img/logo.png')}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ alignContent: 'center', alignItems: 'center' }}>
                        <Text style={VerificationStyle.enterMobileNoTextStyle}> Verification </Text>
                        <Text style={VerificationStyle.enterMobileNoDesc}> {En.opt_desc} </Text>
                        <Text style={VerificationStyle.NoTextStyle}> {this.props.mobile} </Text>
                    </View>
                    <View style={VerificationStyle.inputContainerStyles}>
                        <OtpInputs
                            handleChange={(otp) => this.setState({  otp })}
                            numberOfInputs={4}
                            autoFocusOnLoad
                            //   inputStyles={VerificationStyle.otpInputStyle}
                            inputContainerStyles={VerificationStyle.otpInputContainer}
                            //   inputsContainerStyles={VerificationStyle.otpInputsContainers}
                            containerStyles={VerificationStyle.otpContainer}
                            focusedBorderColor={Colors.primary_dark}
                        />



                    </View>

                    <View style={VerificationStyle.inputContainer}>
                        <SvgIcon
                            style={VerificationStyle.inputIcon}
                            name='password'
                            size={18}
                            color='gray'
                        />
                        <TextInput style={VerificationStyle.inputs}
                            placeholder={En.new_password}
                            secureTextEntry={true}
                            underlineColorAndroid='transparent'
                            onChangeText={(newPassword) => this.setState({ newPassword })

                            } />
                    </View>

                    <View style={VerificationStyle.inputContainer}>
                        <SvgIcon
                            style={VerificationStyle.inputIcon}
                            name='password'
                            size={18}
                            color='gray'
                        />
                        <TextInput style={VerificationStyle.inputs}
                            placeholder={En.confirm_password}
                            secureTextEntry={true}
                            underlineColorAndroid='transparent'
                            error={"value is mandatory"}
                            autoCorrect={true}
                            onChangeText={(confirmPassword) => this.setState({ confirmPassword })} />
                    </View>

                    <TouchableHighlight style={[VerificationStyle.buttonContainer, VerificationStyle.loginButton]} onPress={() => this.onClickListener('login')}>
                    {this.state.isLoading ? (
                  <ActivityIndicator size="small" color="white" />
                ) : (
                    <Text style={VerificationStyle.loginText}>Verify</Text>
                )}
                        
                    </TouchableHighlight>
                    <View style={VerificationStyle.alradyHaveAccountStyle}>
                        <TouchableHighlight onPress={() => this.onClickListener('verify')}>
                            <Text style={VerificationStyle.alradyHaveTxtStyle2}>Re Send OTP</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ImageBackground>
        );
    }



    componentWillUnmount() {
        RNOtpVerify.removeListener();
    }
}


