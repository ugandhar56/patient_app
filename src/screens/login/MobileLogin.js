import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    ImageBackground,
    Alert,
    CheckBox
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../../selection.json';
const SvgIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
// import { Colors } from 'react-native/Libraries/NewAppScreen';

export default class MobileLogin extends Component {

    constructor(props) {
        super(props);
        state = {
            email: '',
            password: '',
            checked: false,
        }
    }

    onClickListener = (viewId) => {
        Alert.alert("Alert", "Button pressed " + viewId);
    }

    render() {
        return (
            <ImageBackground source={require('../../../assets/img/login_bac2.png')} style={styles.backgroundImage} >
                <View style={styles.container}>
                    <View >
                        <Image style={styles.iConStyle}

                            source={require('../../../assets/img/logo.png')}
                            resizeMode="contain"
                        />
                    </View>
                    <View>
                        <Text style={styles.enterMobileNoTextStyle}> Enter Your mobile Number to Login</Text>
                    </View>
                    <View style={styles.inputContainer}>
                    <SvgIcon
                    style={styles.inputIcon}
                   name='user'
          size={18}
          color='gray'
        />
                        {/* <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/message/ultraviolet/50/3498db' }} /> */}
                        <TextInput style={styles.inputs}
                            placeholder="Mobile Number"
                            keyboardType="number-pad"
                            underlineColorAndroid='transparent'
                            onChangeText={(email) => this.setState({ email })} />
                    </View>
                    <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener('login')}>
                        <Text style={styles.loginText}>LOGIN</Text>
                    </TouchableHighlight>
                    <View style={styles.alradyHaveAccountStyle}>
                        <Text style={styles.alradyHaveTxtStyle}>Already have an Account?</Text>
                        <TouchableHighlight onPress={() => this.onClickListener('signUp')}>
                            <Text style={styles.alradyHaveTxtStyle2}>LOGIN</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#DCDCDC',

    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,

    },
    imageContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#000000',
        width: 30,
        height: 30,
        alignItems: 'center',
        flexDirection: 'column'
    },
    inputContainer: {
    
        borderColor:'blue',
        backgroundColor: '#FFFFFF',
        borderRadius: 25,
        borderWidth: 1,
        height: 55,
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
        flexDirection: 'row',
        // alignItems: 'center'
    },
    inputs: {
        height: 55,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    iConStyle: {
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        margin: 10,
        width: 150,
        height: 85

    },
    enterMobileNoTextStyle:{
         marginTop: 30,
          marginBottom: 15,
          color: 'black',
        fontSize: 15, 
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 17,
        justifyContent: 'center'
    },
    alradyHaveAccountStyle: {
        flexDirection: 'row',
        width: '100%',
        height:45,
        backgroundColor:'#eeeeee',
        justifyContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute', //Here is the trick
        fontSize: 18,
        bottom: 0,
    },
    alradyHaveTxtStyle: {
        color: 'black',
    },
    alradyHaveTxtStyle2: {
        
        color: 'blue',    
        margin:5,
        textDecorationLine: 'underline',

    },
    buttonContainer: {
        height: 55,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        width:200,
        // alignSelf: 'stretch',
        borderRadius: 25,
    },
    loginButton: {
        backgroundColor: "blue",
    },
    loginText: {
        color: 'white',
        fontSize: 18
    }
});