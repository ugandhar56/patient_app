import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TouchableOpacity, StyleSheet, View, Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import CallCCAButton from './CallCCAButton';
import NotificationsButton from './NotificationsButton';
import Styles from '../utils/Styles';

const HeaderRightButtons = ({...props}) => (
  <View style={styles.headerRightView}>
    {!props.call ? <CallCCAButton /> : null}
    {props.notifications ? <NotificationsButton /> : null}
    {props.cart ? (
      <TouchableOpacity
        onPress={() => {
          Actions.push('cart');
        }}>
        <View style={Styles.toolbarIconView}>
          <Icon name="shopping-cart" size={25} color="white" />
          <View style={Styles.toolbarCountView}>
            <Text style={Styles.toolbarCountText}>
              {props.cartCount != undefined
                ? props.cartCount
                : global.cartCount}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ) : null}
    {props.home ? (
      <TouchableOpacity
        onPress={() => {
          Actions.push('home');
        }}>
        <Icon
          style={Styles.toolbarIconView}
          name="home"
          size={25}
          color="white"
        />
      </TouchableOpacity>
    ) : null}
  </View>
);

const styles = StyleSheet.create({
  headerRightView: {
    flexDirection: 'row',
    marginLeft: 5,
    marginRight: 5,
  },
});

export default HeaderRightButtons;
