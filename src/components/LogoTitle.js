import React from 'react';
import {Image, StyleSheet, View} from 'react-native';

const LogoTitle = ({...props}) => (
  <View style={styles.logoView}>
    <Image
      style={styles.logoImage}
      source={require('../../assets/images/logo.png')}
      resizeMode="contain"
    />
  </View>
);

const styles = StyleSheet.create({
  logoView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImage: {
    width: 120,
    height: 90,
  },
});

export default LogoTitle;
