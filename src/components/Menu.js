import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function Menu({
  onItemSelected,
  firstName,
  lastName,
  data,
  profilePic,
  profileClick,
  logoutClick,
}) {
  return (
    <View style={styles.menu}>
      <TouchableOpacity onPress={profileClick} style={styles.profileTouchView}>
        <View style={styles.avatarContainer}>
          {profilePic == null ? (
            <Icon name="account-circle" size={48} color="black" />
          ) : (
            <Image
              style={styles.avatar}
              source={{uri: 'data:image/png;base64,' + profilePic}}
            />
          )}
          <Text style={styles.name}>
            {firstName} {lastName}
          </Text>
        </View>
      </TouchableOpacity>
      <FlatList
        style={{flex: 1}}
        data={data}
        renderItem={({item}) => (
          <TouchableOpacity onPress={() => onItemSelected(item)}>
            <View style={styles.item}>
              <Image style={styles.itemImage} source={item.image} />
              <Text style={styles.itemText}>{item.name}</Text>
            </View>
          </TouchableOpacity>
        )}
      />

      <View style={styles.divider} />
      <TouchableOpacity onPress={logoutClick}>
        <View style={styles.logoutView}>
          <Icon name="exit-to-app" size={25} />
          <Text style={styles.logoutText}>Logout</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

Menu.propTypes = {
  onItemSelected: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
  },
  profileTouchView: {
    marginBottom: 10,
  },
  avatarContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
  },
  name: {
    flex: 1,
    fontSize: 18,
    marginLeft: 5,
    alignSelf: 'center',
    flexWrap: 'wrap',
    color: 'black',
    fontWeight: 'bold',
  },
  item: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  itemText: {
    fontSize: 16,
    flexWrap: 'wrap',
    marginLeft: 10,
    textAlignVertical: 'center',
    flex: 1,
  },
  itemImage: {
    width: 25,
    height: 25,
    alignSelf: 'center',
  },
  divider: {
    borderWidth: 0.5,
    borderColor: 'grey',
    marginTop: 5,
  },
  logoutText: {
    fontSize: 16,
    marginLeft: 10,
  },
  logoutView: {
    flexDirection: 'row',
    marginTop: 10,
  },
});
