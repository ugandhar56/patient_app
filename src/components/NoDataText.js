import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

const NoDataText = ({text, ...props}) => (
  <View style={styles.noDataView}>
    <Text style={styles.noDataText}>{text}</Text>
  </View>
);

const styles = StyleSheet.create({
  noDataView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noDataText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
    margin: 10,
    textAlign: 'center',
  },
});

export default NoDataText;
