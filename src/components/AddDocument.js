import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal
} from 'react-native';
import En from '../utils/En';
import Colors from '../utils/Colors';
import ModalDropdown from './ModalDropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as AppConstants from '../utils/AppConstants';

const AddDocument = ({
  documentResponse,
  departmentSelection,
  onSubmit,
  onCancel,
}) => ( <
  Modal transparent = {
    true
  } >
  <
  View style = {
    styles.fileModalView
  } >
  <
  View style = {
    styles.fileSelectionView
  } > {
    documentResponse != null ? ( <
      Text style = {
        styles.selectedFileText
      } > {
        documentResponse.fileName
      } <
      /Text>
    ) : null
  } <
  View style = {
    styles.dataDropDownView
  } >
  <
  ModalDropdown style = {
    Styles.dataDropDown
  }
  textStyle = {
    styles.dataDropDownText
  }
  dropdownTextStyle = {
    styles.dataDropDownItemText
  }
  options = {
    AppConstants.DOCUMENT_TYPES.map(item => item.value)
  }
  defaultValue = {
    En.report_type
  }
  onSelect = {
    (index, value) => {
      departmentSelection(value);
    }
  }
  adjustFrame = {
    style => {
      style.right = 10;
      return style;
    }
  }
  /> <
  Icon name = "arrow-drop-down"
  color = "gray"
  size = {
    22.5
  }
  /> <
  /View> <
  View style = {
    styles.buttonsView
  } >
  <
  Text onPress = {
    onSubmit
  }
  style = {
    styles.confirmButton
  } > {
    En.submit
  } <
  /Text> <
  Text onPress = {
    onCancel
  }
  style = {
    styles.cancelButton
  } > {
    En.cancel
  } <
  /Text> <
  /View> <
  /View> <
  /View> <
  /Modal>
);

const styles = StyleSheet.create({
  confirmButton: {
    color: 'white',
    backgroundColor: Colors.primary_dark,
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
    paddingLeft: 20,
    borderRadius: 10,
    marginRight: 5,
  },
  selectedFileText: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  fileModalView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  cancelButton: {
    color: 'white',
    backgroundColor: 'dimgray',
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
    paddingLeft: 20,
    borderRadius: 10,
    marginLeft: 5,
  },
  fileSelectionView: {
    backgroundColor: 'white',
    borderRadius: 10,
    elevation: 20,
    shadowColor: 'black',
    padding: 20,
    margin: 20,
  },
  dataDropDownView: {
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  dataDropDownText: {
    fontSize: 16,
    color: Colors.text_input,
  },
  dataDropDownItemText: {
    fontSize: 16,
    color: Colors.text_input_placeholder,
  },
  buttonsView: {
    alignSelf: 'center',
    marginTop: 10,
    flexDirection: 'row',
  },
});

export default AddDocument;