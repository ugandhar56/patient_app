import React, {Component} from 'react';
import {View, WebView, StyleSheet, Text} from 'react-native';
import {
  handleAndroidBackButton,
  removeAndroidBackButtonHandler,
} from './AndroidBackHandling';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Actions} from 'react-native-router-flux';
import LoadingIndicator from './LoadingIndicator';

export default class Webview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: props.url,
      isLoading: true,
    };
  }

  componentDidMount() {
    handleAndroidBackButton(true);
  }

  componentWillUnmount() {
    removeAndroidBackButtonHandler();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.urlView}>
          <Icon
            name="close"
            size={25}
            color="gray"
            style={styles.closeIcon}
            onPress={() => {
              Actions.pop();
            }}
          />
          <Text numberOfLines={1} style={styles.urlText}>
            {this.state.url}
          </Text>
        </View>
        <View style={styles.divider} />
        <WebView
          source={{uri: this.state.url}}
          onLoadStart={() => this.setState({isLoading: true})}
          onLoadEnd={() => this.setState({isLoading: false})}
        />
        {this.state.isLoading ? <LoadingIndicator /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  divider: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    width: '100%',
    marginTop: 5,
    marginBottom: 5,
  },
  urlView: {
    flexDirection: 'row',
    padding: 5,
  },
  closeIcon: {
    alignSelf: 'center',
  },
  urlText: {
    fontSize: 18,
    marginLeft: 10,
    alignSelf: 'center',
    flex: 1,
  },
});
