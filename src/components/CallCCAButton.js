import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Linking} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LoadingIndicator from './LoadingIndicator';
import Toast from 'react-native-simple-toast';
import * as AppConstants from '../utils/AppConstants';
import Helpers from '../utils/Helpers';
import ShowError from './ShowError';
import Colors from '../utils/Colors';

export default class CallCCAButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  getCCANumber = () => {
    this.setState({isLoading: true});
    fetch(AppConstants.BASE_URL + 'customer/support', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        sessionId: global.sessionId,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({isLoading: false});
        if (responseJson.status == AppConstants.STATUS_OK) {
          Linking.openURL(`tel:${responseJson.data}`);
        } else if (responseJson.status == AppConstants.STATUS_INVALID) {
          Toast.show(responseJson.message);
          Helpers.redirectToLogin();
        } else {
          Toast.show(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        ShowError(error);
      });
  };

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.getCCANumber();
          }}
          style={styles.callIconTouchView}>
        
          <Icon name="call" size={15} color="black" />
        </TouchableOpacity>
        {this.state.isLoading ? <LoadingIndicator /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  callIconTouchView: {
    borderRadius: 12.5,
    height: 25,
    width: 25,
    backgroundColor: Colors.call_icon,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 5,
  },
});
