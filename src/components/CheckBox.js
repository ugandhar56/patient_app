import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

const CheckBox = ({
  selected,
  onPress,
  style,
  textStyle,
  size = 25,
  color = '#211f30',
  text = '',
  ...props
}) => (
  <TouchableOpacity
    style={[styles.checkBox, style]}
    onPress={onPress}
    {...props}>
    <Icon
      size={size}
      color={color}
      name={selected ? 'check-box' : 'check-box-outline-blank'}
    />

    <Text style={[textStyle, styles.boxTextStyle]}>{text}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  boxTextStyle: {
    marginLeft: 5,
  },
});

export default CheckBox;
