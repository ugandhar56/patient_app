import SocketIOClient from 'socket.io-client';
import {AsyncStorage} from 'react-native';
import * as AppConstants from '../utils/AppConstants';

export default class SocketManager {
  constructor() {
    this.socket = SocketIOClient(AppConstants.SOCKET_URL, {
      transports: ['websocket'],
    });

    this.socket.on('connect', () => {
      AsyncStorage.getItem('sessionId')
        .then(sessionId => {
          AsyncStorage.getItem('id').then(customerId => {
            this.registertonodejs(customerId, sessionId);
          });
        })
        .done();
    });

    this.socket.on('online', userId0 => {});

    this.socket.on('offline', userId1 => {});

    this.socket.on('disconnect', () => {});
  }

  sendUserOffline() {
    this.socket.emit('makeMeOffline', {
      userId: global.customerId,
    });
  }

  async getallUsers(customerId, sessionId) {
    try {
      let response = await fetch(
        AppConstants.BASE_URL +
          'videocall/customerdoctors?customerid=' +
          JSON.parse(customerId),
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            SessionId: JSON.parse(sessionId),
          },
        },
      );
      let responseJson = await response.json();
      return responseJson.doctors;
    } catch (error) {
      ShowError(error);
    }
  }

  // customerId as parameter to push all doctorIDs to Server.Socket.IO
  registertonodejs(customerId, sessionId) {
    this.getallUsers(customerId, sessionId).then(x => {
      try {
        let JsonValue = x;
        let assocRelatedUsersList = [];
        let associatedDoctorsList = [];
        for (let i = 0; i < JsonValue.length; i++) {
          assocRelatedUsersList.push(JsonValue[i].doctorId);
          associatedDoctorsList[JsonValue[i].doctorId] = JsonValue[i];
        }
        this.socket.emit('adduser', {
          userId: JSON.parse(customerId),
          userName: 'doctor',
        });

        this.socket.emit('updateRelatedUsersList', assocRelatedUsersList);
        this.socket.emit('getUserStatus', JSON.parse(customerId));
        this.socket.on('user_status', function(st) {
          status = st;
        });
      } catch (error) {}
    });
  }
}
