import React from 'react';
import {TouchableOpacity, StyleSheet, View} from 'react-native';
import Colors from '../utils/Colors';

const RadioButton = ({...props}) => (
  <TouchableOpacity style={styles.radioCircle} onPress={props.onPress}>
    {props.selected ? <View style={styles.radioInner} /> : null}
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  radioCircle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: Colors.primary_dark,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginRight: 10,
  },
  radioInner: {
    height: 10,
    width: 10,
    borderRadius: 6,
    backgroundColor: Colors.primary_dark,
  },
});

export default RadioButton;
