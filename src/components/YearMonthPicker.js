import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Modal,
  TextInput,
} from 'react-native';
import Colors from '../utils/Colors';
import ModalDropdown from './ModalDropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import En from '../utils/En';

export default class YearMonthPicker extends Component {
  constructor(props) {
    super(props);
    let {visiable, placeholder, onConfirmed, pickerDisable, onClicked} = props;
    let years = this.getYears();
    let months = this.getMonths();
    this.state = {
      years,
      months,
      selectedYear: '',
      selectedMonth: '',
      visiable: visiable || false,
      placeholder,
      selectedDate: '',
      onConfirmed,
      pickerDisable,
      onClicked,
    };
  }

  show() {
    this.setState({
      visiable: true,
      selectedYear: '',
      selectedMonth: '',
    });
  }

  confirm = () => {
    if (
      this.state.onConfirmed(
        this.state.selectedMonth + ' - ' + this.state.selectedYear,
      )
    ) {
      this.setState({
        selectedDate:
          this.state.selectedMonth + ' - ' + this.state.selectedYear,
      });
      this.dismiss();
    }
  };

  dismiss = () => {
    this.setState({
      visiable: false,
    });
  };

  getYears = () => {
    let years = [];
    for (let i = new Date().getFullYear(); i >= 1970; i--) {
      years.push(i);
    }
    return years;
  };

  getMonths = () => {
    let months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    return months;
  };

  onCancelPress = () => {
    this.dismiss();
  };

  onConfirmPress = () => {
    if (this.state.selectedMonth === '' && this.state.selectedYear != '') {
      Toast.show(En.please_select_month, Toast.SHORT);
    } else if (
      this.state.selectedMonth !== '' &&
      this.state.selectedYear === ''
    ) {
      Toast.show(En.please_select_year, Toast.SHORT);
    } else if (
      this.state.selectedMonth === '' &&
      this.state.selectedYear === ''
    ) {
      Toast.show(En.please_select_month_and_year, Toast.SHORT);
    } else if (
      this.state.selectedMonth !== '' &&
      this.state.selectedYear !== ''
    ) {
      //selected date
      let sd = moment(
        this.state.selectedMonth + ' - ' + this.state.selectedYear,
        'MMM - YYYY',
        true,
      ).format();
      //current date
      let cd = moment(
        moment(new Date()).format('MMM - YYYY'),
        'MMM - YYYY',
        true,
      ).format();
      if (moment(sd).isSameOrBefore(cd)) {
        this.confirm();
      } else {
        Toast.show(En.selected_month_exceed);
      }
    }
  };

  render() {
    const {
      years,
      months,
      visiable,
      placeholder,
      selectedDate,
      pickerDisable,
      onClicked,
    } = this.state;
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            if (onClicked()) {
              this.show();
            }
          }}
          disabled={pickerDisable}>
          <TextInput
            style={styles.dataTextInput}
            underlineColorAndroid="transparent"
            multiline={false}
            placeholder={placeholder}
            placeholderTextColor={Colors.text_input_placeholder}
            editable={false}
            value={selectedDate}
          />
        </TouchableOpacity>
        <Modal transparent={true} visible={visiable}>
          <View style={styles.modal}>
            <View style={styles.outerContainer}>
              <View style={styles.toolBar}>
                <TouchableOpacity
                  style={styles.toolBarButton}
                  onPress={this.onCancelPress}>
                  <Text style={styles.toolBarButtonText}>Cancel</Text>
                </TouchableOpacity>
                <View style={{flex: 1}} />
                <TouchableOpacity
                  style={styles.toolBarButton}
                  onPress={this.onConfirmPress}>
                  <Text style={styles.toolBarButtonText}>Ok</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.innerContainer}>
                <View style={styles.dataDropDownView}>
                  <ModalDropdown
                    style={styles.dataDropDown}
                    textStyle={[styles.dataDropDownText]}
                    dropdownTextStyle={styles.dataDropDownItemText}
                    options={months.map(item => item)}
                    defaultValue="MMM"
                    onSelect={(index, value) => {
                      this.setState({selectedMonth: value});
                    }}
                    adjustFrame={style => {
                      style.right = 10;
                      return style;
                    }}
                  />
                  <Icon
                    name="keyboard-arrow-down"
                    color={Colors.text_input_placeholder}
                    size={18}
                    style={styles.dropdownArrow}
                  />
                </View>
                <View style={styles.dataDropDownView}>
                  <ModalDropdown
                    style={styles.dataDropDown}
                    textStyle={[styles.dataDropDownText]}
                    dropdownTextStyle={styles.dataDropDownItemText}
                    options={years.map(item => item)}
                    defaultValue="YYYY"
                    onSelect={(index, value) => {
                      this.setState({selectedYear: value});
                    }}
                    adjustFrame={style => {
                      style.right = 10;
                      return style;
                    }}
                  />
                  <Icon
                    name="keyboard-arrow-down"
                    color={Colors.text_input_placeholder}
                    size={18}
                    style={styles.dropdownArrow}
                  />
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  outerContainer: {
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 10,
  },
  toolBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 44,
  },
  toolBarButton: {
    height: 44,
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  toolBarButtonText: {
    fontSize: 15,
    color: '#2d4664',
  },
  innerContainer: {
    flexDirection: 'row',
  },
  picker: {
    flex: 1,
  },
  dataTextInput: {
    color: Colors.text_input,
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 5,
    fontSize: 16,
  },
  dataDropDownView: {
    flexDirection: 'row',
    borderRadius: 5,
    borderColor: 'black',
    borderWidth: 0.5,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5,
    flex: 1,
  },
  dataDropDown: {
    flex: 1,
  },
  dataDropDownText: {
    fontSize: 16,
    color: Colors.text_input,
  },
  dataDropDownIcon: {
    marginRight: 5,
    alignSelf: 'center',
  },
  dataDropDownItemText: {
    fontSize: 16,
    color: 'gray',
  },
});
