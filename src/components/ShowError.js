import Toast from 'react-native-simple-toast';
import En from '../utils/En';

const ShowError = error => {
  if (error.toString() === 'TypeError: Network request failed') {
    Toast.show(En.check_internet, Toast.LONG);
  } else {
    Toast.show(error.toString(), Toast.LONG);
  }
};

export default ShowError;
