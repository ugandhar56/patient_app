import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Styles from '../utils/Styles';
import {Actions} from 'react-native-router-flux';
import {inject, observer} from 'mobx-react';

const NotificationsButton = ({...props}) => (
  <TouchableOpacity
    onPress={() => {
      Actions.push('notifications');
    }}>
    <View style={Styles.toolbarIconView}>
      <Icon name="notifications" size={25} color="white" />
      {props.notificationStore.count != 0 ? (
        <View style={Styles.toolbarCountView}>
          <Text style={Styles.toolbarCountText}>
            {props.notificationStore.count}
          </Text>
        </View>
      ) : null}
    </View>
  </TouchableOpacity>
);

export default inject('notificationStore')(observer(NotificationsButton));
