import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {StyleSheet, View, Platform} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Colors from '../utils/Colors';

const HeaderLeftButtons = ({...props}) => (
  <View style={styles.leftHeaderView}>
    <Icon
      style={styles.backArrowIcon}
      name={Platform.OS === 'ios' ? 'arrow-back-ios' : 'arrow-back'}
      size={25}
      color={props.transparent == true ? Colors.primary : 'white'}
      onPress={() => {
        if (
          props.recievedParams != undefined &&
          props.recievedParams.allowBack == false
        ) {
          if (props.sendingParams != undefined) {
            Actions.push(props.screen, props.sendingParams);
          } else {
            Actions.push(props.screen);
          }
        } else {
          Actions.pop();
        }
      }}
    />
  </View>
);

const styles = StyleSheet.create({
  leftHeaderView: {
    flexDirection: 'row',
  },
  backArrowIcon: {
    marginLeft: 10,
  },
});

export default HeaderLeftButtons;
