import React from 'react';
import {View, ActivityIndicator, StyleSheet, Modal} from 'react-native';
import Colors from '../utils/Colors';

const LoadingIndicator = () => (
  <Modal transparent={true}>
    <View style={styles.loadingView}>
      <ActivityIndicator
        size="large"
        color="white"
        style={styles.loadingIndicator}
      />
    </View>
  </Modal>
);

const styles = StyleSheet.create({
  loadingView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingIndicator: {
    backgroundColor: Colors.primary_dark,
    padding: 10,
    borderRadius: 10,
  },
});

export default LoadingIndicator;
