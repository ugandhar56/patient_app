// packages
import {BackHandler} from 'react-native';
import {Actions} from 'react-native-router-flux';
/**
 * Attaches an event listener that handles the android-only hardware
 * back button
 * @param  {boolean} goBack to exit or pop back
 */
const handleAndroidBackButton = (goBack, recievedParams, screen) => {
  BackHandler.addEventListener('hardwareBackPress', () => {
    if (goBack) {
      if (recievedParams != undefined) {
        Actions.push(screen);
      } else {
        Actions.pop();
      }
    } else {
      BackHandler.exitApp();
    }
    return goBack;
  });
};
/**
 * Removes the event listener in order not to add a new one
 * every time the view component re-mounts
 */
const removeAndroidBackButtonHandler = () => {
  BackHandler.removeEventListener('hardwareBackPress', () => {});
};
export {handleAndroidBackButton, removeAndroidBackButtonHandler};
