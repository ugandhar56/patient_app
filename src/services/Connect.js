import {
  create
} from 'apisauce';
import {
  BaseURL,METHOD_LOGIN,METHOD_OTP_REGISTER,AUTHORIZATION_KEY
} from '../config/serviceURLS';
import axios from 'axios'
import clUtilities from '../config/Utilities';
import clIConstants from '../config/Constants';
import { connect } from 'socket.io-client';
import {decode as atob, encode as btoa} from 'base-64'

// const Connect = create({
//   /**
//    * Import the config from the App/Config/index.js file
//    */
//   baseURL: BaseURL,
//   headers: {
//     Accept: 'application/json',
//     'Content-Type': 'application/json',
//   },
//   timeout: 60000,
// });


function sendLoginRequest( reqParms,
  responceHandler,
  errorHandler,)
{
  const userName = reqParms.getParts().find(item => item.fieldName === 'username');
    const password = reqParms.getParts().find(item => item.fieldName === 'password');
    console.log("=== user ======="+ userName.string);
    console.log("=== password ======="+ password.string);
    console.log("=== login params ==" +JSON.stringify(reqParms) )

    axios.post(BaseURL+"/"+METHOD_LOGIN,reqParms,{
      headers: {
        'Authorization': AUTHORIZATION_KEY, 
        'x-Skip-Interceptor':"",
        // 'Content-Type': 'application/json; charset=utf-8'
      },
    // }).
    // axios.post(BaseURL+"/"+METHOD_LOGIN,JSON.stringify(reqParms),{
    //   headers:{'Authorization': 'Basic bWVkMzYwLWN1c3RvbWVyOndoYlp0SEtJNHY=',}
  //   })
  // axios.post(BaseURL+"/"+METHOD_LOGIN,JSON.stringify(reqParms),{
  //   headers: {
  //     'Authorization': 'Basic bWVkMzYwLWN1c3RvbWVyOndoYlp0SEtJNHY=', 
  //     'x-Skip-Interceptor':"",
  //     // 'Content-Type': 'application/json; charset=utf-8'
  //   },
  }).then(function(result) {
      console.log('Logging result ' + JSON.stringify(result));
      let data = result.data;
      responceHandler(1,"sucess",data)
    })
    .catch(function(error) {
      console.log('What happened? ' + JSON.stringify(error));
      alert("error   "+error.message)
      errorHandler(-1)
    });
  
}


function verifyOtpWithPWD( sVerifyJson,
  responceHandler,
  errorHandler,)
{
  console.log('/// verifyOtpWithPWD ///' + sVerifyJson);
  console.log('sURL:  '+BaseURL+"/"+METHOD_OTP_REGISTER);
  axios.post(BaseURL+"/"+METHOD_OTP_REGISTER,sVerifyJson,{
    headers:{'Content-Type': 'application/json; charset=utf-8'}
  })
  .then(function(result) {
      console.log('Verify pwd result ' + JSON.stringify(result));
      let data = result.data;
      responceHandler(1,"sucess",data)
    })

    .catch(function(error) {
      console.log('What happened? ' + JSON.stringify(error));
      alert("error   "+error.message)
      errorHandler(-1)
    });
  
}


function sendPostRequest(
  iMethodType,
  reqParms,
  responceHandler,
  sSessionId,
  errorHandler,
) {
  var sData;
  if(iMethodType === clIConstants.MethodTypes.LOGIN)
  {
    const userName = reqParms.getParts().find(item => item.fieldName === 'username');
    const password = reqParms.getParts().find(item => item.fieldName === 'password');
    console.log('headers  ================ name  ' + JSON.stringify('Basic '+btoa(userName.string,password.string)));
    // console.log('headers  ================ password  ' +JSON.stringify( password.string));

   
    Connect.setHeader(new Headers({
      'Authorization': 'Basic '+btoa(userName.string,password.string), 
      // 'Content-Type': 'application/x-www-form-urlencoded',
      Accept: '*/*',
      'Content-Type': 'multipart/form-data'
    }), )
  }
  else
  {
  Connect.setHeader('sessionid', sSessionId);
  }
  console.log('url  ================' + BaseURL + clUtilities.getUrl(iMethodType));
  console.log('parms ==============' + JSON.stringify(reqParms));
  console.log('headers ==============' + JSON.stringify(Connect.headers));
  Connect.post(clUtilities.getUrl(iMethodType), reqParms)
    .then((response) => {
      if (response.ok) {
        //- True is the status code is in the 200's; false otherwise.
        sData = response.data;
        console.log('sDAta  =============' + JSON.stringify(response));
        if (sData != null) {
          var iStatus = sData.Result.lResult;
          var sMessage = sData.Result.sValue;

          sData = sData.arrData;
          // alert("Service responce : "+JSON.stringify(response));

          responceHandler(iStatus, sMessage, sData);
        } else {
          errorHandler(-1);
        }
      } else {
        console.log(JSON.stringify(response))
        alert(
          'connection error    \n' +
          JSON.stringify(response),
        );
        errorHandler(-1);
      }
    })
    .catch((err) => {
      alert('error  ' + err);
      errorHandler(-1);
    });
}

function sendGetRequest(url, handler) {
  let sData;
  console.log("url ==========" + BaseURL+url)

Connect.get(BaseURL+url)
    .then((response) => {
      if (response.ok) {
        //- True is the status code is in the 200's; false otherwise.
        alert('Service responce : ' + response.data);
        sData = response.data;
        handler(sData, iStauts);
      } else {
        alert(
          'error   ' + JSON.stringify(response.originalError.request._response),
        );
      }
    })
    .catch((err) => {
      console.log('error==' ,err.text)
      alert('error  ' + err.text);
    });
}

function getFectchRequest(url,handler){

  console.log("url =============" + url)
                fetch(url, {
                  method: "GET",
                  headers: {
                    Accept: "*/*",
                    // "Content-Type": "application/json",
                  },
                })
                  .then((response) => response.json())
                  .then((responseJson) => {
                    console.log(("========sucess ===="+JSON.stringify(responseJson)))
                    // this.setState({ isLoading: false });
                    if (responseJson.statusCode == 200) {
                      console.log("call=== back ====")
                      handler(responseJson.body);
                    } else {
                     console.log("message===="+responseJson.message);
                    }
                  })
                  .catch((error) => {
                      console.log(("======error ======"+error.toString()))
                    // ShowError(error);
                    // this.setState({ isLoading: false });
                  });
}
function postFctchRequest(url,params,handler){

  console.log("url =============" + url)
                fetch(url, {
                  method: "POST",
                  // headers: {
                  //   // Accept: "*/*",
                  //   "Content-Type": "application/json",
                  // },
                  body:params
                })
                  .then((response) => response.json())
                  .then((responseJson) => {
                    console.log(("========sucess ===="+JSON.stringify(responseJson)))
                    // this.setState({ isLoading: false });
                    if (responseJson.statusCode == 200) {
                      console.log("call=== back ====")
                      handler(responseJson.body);
                    } else {
                     console.log("message===="+responseJson.message);
                    }
                  })
                  .catch((error) => {
                      console.log(("======error ======"+error.toString()))
                    // ShowError(error);
                    // this.setState({ isLoading: false });
                  });
}


export const serverConnection = {
  sendPostRequest,
  sendGetRequest,
  getFectchRequest,
  ////////login ////////
  sendLoginRequest,
  verifyOtpWithPWD
}