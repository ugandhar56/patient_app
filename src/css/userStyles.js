import { Dimensions, StyleSheet } from 'react-native';
export default userStyles = StyleSheet.create({
    sixteen_bold: {
        fontSize: 16,
        fontFamily: "OpenSans-Bold",
        color: "white",
        lineHeight: 22
    },
    sixteen_regular: {
        fontSize: 16,
        fontFamily: "OpenSans-Regular",
        color: "white",
        lineHeight: 22
    },
    sixteen_semi_bold: {
        fontSize: 16,
        fontFamily: "OpenSans-SemiBold",
        color: "white",
        lineHeight: 22
    },

    fourteen_bold: {
        fontSize: 14,
        fontFamily: "OpenSans-Bold",
        color: "white",
        lineHeight: 19
    },
    fourteen_regular: {
        fontSize: 14,
        fontFamily: "OpenSans-Regular",
        color: "white",
        lineHeight: 19
    },
    fourteen_semi_bold: {
        fontSize: 14,
        fontFamily: "OpenSans-SemiBold",
        color: "white",
        lineHeight: 19
    },

    view_background: { flex: 1, backgroundColor: "white" },
    screen_background: { flex: 1, backgroundColor: "#d4e3e9" }
})