
import { Dimensions, StyleSheet } from 'react-native';

const VerificationStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // alignSelf:'center'
        // backgroundColor: '#DCDCDC',

    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,

    },
    imageContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#000000',
        width: 30,
        height: 30,
        alignItems: 'center',
        flexDirection: 'column'
    },
    inputContainerStyles: {
        height: 50,
        alignContent: 'center',
        alignItems: 'center',
        margin:5,
    
    },
    inputs: {
        height: 55,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    iConStyle: {
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        margin: 10,
        width: 150,
        height: 85

    },
    enterMobileNoTextStyle: {
        marginTop: 10,
        marginBottom: 10,
        color: 'blue',
        fontSize: 24,
        alignContent: "center"
    },
    enterMobileNoDesc: {
        marginTop: 5,
        marginBottom: 5,
        color: 'gray',
        fontSize: 14,
    },
    NoTextStyle: {
        marginTop: 10,
        marginBottom: 5,
        color: 'black',
        fontSize: 16,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 17,
        justifyContent: 'center'
    },
    alradyHaveAccountStyle: {
        flexDirection: 'row',
        width: '100%',
        height: 45,
        backgroundColor: '#eeeeee',
        justifyContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute', //Here is the trick
        fontSize: 18,
        bottom: 0,
    },
    alradyHaveTxtStyle: {
        color: 'black',
    },
    alradyHaveTxtStyle2: {

        color: 'blue',
        margin: 5,
        textDecorationLine: 'underline',

    },
    buttonContainer: {
        height: 55,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        width: 200,
        // alignSelf: 'stretch',
        borderRadius: 25,
    },
    loginButton: {
        backgroundColor: "blue",
    },
    loginText: {
        color: 'white',
        fontSize: 18
    },
    otpInputStyle: {
        backgroundColor: "white",
        color: "black",
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderBottomWidth: 1.5,
        borderBottomColor: "#eeeeee",
    },
    otpInputContainer: {
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderBottomWidth: 1.5,
        borderRadius: 0,
        margin: 0,
        marginRight: 20,
    },
    otpInputsContainers: {
        flexDirection: "row",
        justifyContent: "center",
        marginHorizontal: 0,
        marginVertical: 20,
    },
    otpContainer: {
        flex: 0,
        marginTop: 25,
    },
    
  inputContainer: {
  
    borderColor:'blue',
    backgroundColor: '#FFFFFF',
    borderRadius: 25,
    borderWidth: 1,
    //   width:250,
    height: 55,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    
    flexDirection: 'row',
    alignItems: 'center'
},
inputs: {
    height: 55,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
},
iConStyle: {
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    margin: 10,
    width: 150,
    height: 85


},
inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 20,
    marginTop:10,
    justifyContent: 'center'
},
});


export default  VerificationStyle;