export default Dimens = {
  primary_text: 16,
  secondary_text: 14,
  button_text: 18,
  toolbar_icon: 25,
  normal_icon: 18,
  screen_padding: 12,
  toolbar_title: 18,
  secondary_button_text: 14,
  big_toolbar_icon: 35,
};
