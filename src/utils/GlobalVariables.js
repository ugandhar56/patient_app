{
  global.customerId = "";
  global.sessionId = "";
  global.customerFirstName = "";
  global.customerLastName = "";
  global.customerEmail = "";
  global.customerMobileNo = "";
  global.socketConnectedStatus = false;
  global.cartCount = 0;
  global.localNotificationId = "";
  global.remoteNotificationId = "";
}
