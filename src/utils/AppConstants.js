export const STATUS_OK = '200';
export const STATUS_INVALID = '400';
export const STATUS_NOT_FOUND = '404';
export const STATUS_FORBIDDEN = '403';

//export const BASE_URL = "https://www.med360.in/ozn/";
export const BASE_URL = 'https://www.med360.in/ozndev/v1.2/';
//export const BASE_URL = 'http://192.168.0.64:2022/ozn/';
export const AGORA_KEY = '718aa888c8644b4683d52e66e6aa97c1';
export const SOCKET_URL = 'http://www.med360.in:5000';
export const TRACK_URL = 'https://www.med360.in/live/';
export const GOOGLE_MAPS_APIKEY = 'AIzaSyAcni1wZOEPiO4qkAcEHuVnOaPk94Zuwd4';
export const PACKAGE_IMAGE_URL = 'https://www.med360.in/packages/';
//export const RAZORPAY_KEY = "rzp_live_XXdYIhGwlQcSbZ";
export const RAZORPAY_KEY = 'rzp_test_XsSqYajvnbtd52';
export const VIDEO_CALL_JOIN_CHANNEL_DELAY_TIME = 5000;
export const NOTIFICATION_CHANNEL_ID = 'ozone_customer';
export const NOTIFICATION_CHANNEL_NAME = 'Ozone-Customer';

export const USER_TYPE = 'CUSTOMER';

export const GET_MEDICINES_URL = 'https://www.medplusmart.com/';
export const TERMS_AND_CONDITIONS_URL =
  'https://www.med360.in/assets/ozone/terms/terms&conditions.html';
export const HEALTH_INFO_URL =
  'https://www.who.int/health-topics/coronavirus#tab=tab_1';
export const CLINIC_VISITS_URL =
  'https://www.ozonehospitals.com/book_appointment.html';

export const INSTANT_CONSULTATION = 'ic';
export const SPECIALIST_CONSULTATION = 'sc';
export const COVID_HOMECARE_CONSULTATION = 'ch';
export const COVID_CONSULTATION = 'cc';
export const POPULAR_SERVICE = 'ps';

export const RELATIONS = [
  {
    value: 'Mother',
  },
  {
    value: 'Father',
  },
  {
    value: 'Husband',
  },
  {
    value: 'Wife',
  },
  {
    value: 'Son',
  },
  {
    value: 'Daughter',
  },
  {
    value: 'Sister',
  },
  {
    value: 'Brother',
  },
  {
    value: 'Others',
  },
];

export const GENDERS = [
  {
    value: 'Male',
  },
  {
    value: 'Female',
  },
  {
    value: 'Others',
  },
];

export const CANCELLATION_REASONS = [
  {
    value: 'I am not available',
  },
  {
    value: 'I would like to reschedule appointment today',
  },
  {
    value: 'I would like to reschedule appointment for another day',
  },
  {
    value: 'Other',
  },
];

export const CANCELLATION_POLICIES = [
  {
    value:
      'If Schedule Booking is confirmed and patient cancels the booking before 1hr of the schedule service time, refund will be 100%.',
  },
  {
    value:
      'If Patient Confirms the Instant Service booking and up on cancel or no-show by patient, payment done will not be refunded.',
  },
  {
    value:
      'If booking is confirmed and Ozone cancels the appointment due to any non availability and non-possibility of reschedule the booking, refund will be 100%.',
  },
  {
    value:
      'If Schedule booking is confirmed and patient cancels the booking with in 1hr of the schedule service time, refund will be 50%.',
  },
  {
    value:
      'If Payment deducted from patient payment source without booking confirmation, the complete refund will be credited in 7 business working days.',
  },
];

export const DOCUMENT_TYPES = [
  {
    value: 'MRI',
  },
  {
    value: 'CT SCAN',
  },
  {
    value: 'X-RAY',
  },
  {
    value: 'ULTRASOUND',
  },
  {
    value: 'THREADMILL',
  },
  {
    value: 'ECG',
  },
  {
    value: 'ECHO',
  },
  {
    value: 'EEG',
  },
  {
    value: 'MAMOGRAPHY',
  },
  {
    value: 'BONE DENSITY SCAN',
  },
  {
    value: 'PATHOLOGY (LAB TESTS)',
  },
  {
    value: 'BIOCHEMISTRY (LAB TESTS)',
  },
  {
    value: 'MICROBIOLOGY(LAB TESTS)',
  },
  {
    value: 'PRESCRIPTION',
  },
];

export const CONTACT_US_CATEGORIES = [
  {
    value: 'Query',
  },
  {
    value: 'Complaint',
  },
  {
    value: 'Feedback',
  },
  {
    value: 'Suggestion',
  },
];
