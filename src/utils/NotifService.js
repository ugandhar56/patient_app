import firebase from 'react-native-firebase';
import {AsyncStorage, Platform} from 'react-native';
import * as AppConstants from './AppConstants';
import En from './En';
import Colors from './Colors';
import {Actions} from 'react-native-router-flux';
// import NotificationStore from '../appData/NotificationStore';
import moment from 'moment';

export default class NotifService {
  constructor(onNotif) {
    // Create notification channel required for Android devices
    this.createNotificationChannel();

    // Ask notification permission and add notification listener
    this.checkPermission();
    this.notificationId = '1';
    this.onNotification = onNotif;
  }

  createNotificationChannel = () => {
    // Build a android notification channel
    const channel = new firebase.notifications.Android.Channel(
      AppConstants.NOTIFICATION_CHANNEL_ID, // channelId
      AppConstants.NOTIFICATION_CHANNEL_NAME, // channel name
      firebase.notifications.Android.Importance.Max, // channel importance
    )
      .setDescription('Customer notifications')
      .enableVibration(true)
      .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
      .setVibrationPattern([1000, 1000, 1000, 1000, 1000]);

    // Create the android notification channel
    firebase.notifications().android.createChannel(channel);
  };

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      //generating token
      this.getToken();

      // We've the permission
      this.notificationListener = firebase
        .notifications()
        .onNotification(notification => {
          if (notification._notificationId != global.localNotificationId) {
            global.localNotificationId = notification._notificationId;
            let notification_to_be_displayed = new firebase.notifications.Notification(
              {
                data: notification._android._notification._data,
                sound: 'default',
                show_in_foreground: true,
                title: notification.title,
                body: notification.body,
              },
            );

            if (Platform.OS == 'android') {
              notification_to_be_displayed.android
                .setPriority(firebase.notifications.Android.Priority.Max)
                .android.setChannelId(AppConstants.NOTIFICATION_CHANNEL_ID)
                .android.setVibrate([1000, 1000, 1000, 1000, 1000])
                .android.setAutoCancel(true)
                .android.setColor(Colors.primary_dark)
                .android.setSmallIcon('@drawable/ic_stat_notification');
            }

            firebase
              .notifications()
              .displayNotification(notification_to_be_displayed);

            //getting notifications count
            // NotificationStore.getNotificationsCount();
          }
        });

      //listener for onNotificationOpened
      this.notificationOpenedListener();
    } else {
      // user doesn't have permission
      try {
        await firebase.messaging().requestPermission();
      } catch (error) {
        alert(En.enable_notification_permission);
      }
    }
  };

  async getToken() {
    firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          AsyncStorage.setItem('fcmToken', fcmToken);
        }
      });

    firebase.messaging().onTokenRefresh(fcmToken => {
      if (fcmToken) {
        AsyncStorage.setItem('fcmToken', fcmToken);
        AsyncStorage.getItem('id').then(customerId => {
          if (customerId != null) {
            this.sendDeviceToken(JSON.parse(customerId), fcmToken);
          }
        });
      }
    });
  }

  sendDeviceToken(customerId, fcmToken) {
    var inputData = {
      userId: customerId,
      deviceToken: fcmToken,
    };

    fetch(AppConstants.BASE_URL + 'devicetoken', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        sessionId: global.sessionId,
      },
      body: JSON.stringify(inputData),
    });
  }

  notificationOpenedListener() {
    // App was opened by a local notification
    firebase.notifications().onNotificationOpened(notificationOpen => {
      if (notificationOpen.notification.title == En.incoming_video_call) {
        this.onNotification(notificationOpen.action);
      } else {
        this.checkForDuplicateNotifications(notificationOpen);
      }
    });

    // App was opened by a fcm notification
    firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        this.checkForDuplicateNotifications(notificationOpen);
      });
  }

  checkForDuplicateNotifications(notificationOpen) {
    if (
      notificationOpen != null &&
      global.remoteNotificationId !=
        notificationOpen.notification._notificationId
    ) {
      global.remoteNotificationId =
        notificationOpen.notification._notificationId;
      this.redirectToNotificationScreen();
    }
  }

  redirectToNotificationScreen() {
    AsyncStorage.getItem('id').then(customerId => {
      if (customerId != null) {
        if (Actions.currentScene == 'notifications') {
          Actions.refresh({key: moment()});
        } else {
          Actions.push('notifications', {allowBack: false});
        }
      } else {
        Actions.push('login');
      }
    });
  }

  sendVideoCallNotification = () => {
    this.notificationId = (parseInt(this.notificationId) + 1).toString();
    const notification = new firebase.notifications.Notification({
      show_in_foreground: true,
    })
      .setNotificationId(this.notificationId)
      .setTitle(En.incoming_video_call)
      .setBody(En.video_call_from_doctor)
      .android.setAutoCancel(false)
      .android.setPriority(firebase.notifications.Android.Priority.Max)
      .android.setChannelId(AppConstants.NOTIFICATION_CHANNEL_ID)
      .android.setSmallIcon('@drawable/ic_stat_notification')
      .android.setOngoing(true)
      .android.setColor(Colors.primary_dark)
      .android.setVibrate([1000, 1000, 1000, 1000, 1000]);

    const acceptAction = new firebase.notifications.Android.Action(
      'accept',
      'ic_launcher',
      'Accept',
    );
    notification.android.addAction(acceptAction);

    const rejectAction = new firebase.notifications.Android.Action(
      'reject',
      'ic_launcher',
      'Reject',
    );
    notification.android.addAction(rejectAction);

    firebase.notifications().displayNotification(notification);
  };

  cancelVideoCallNotification = () => {
    firebase.notifications().removeDeliveredNotification(this.notificationId);
  };
  cancelAllNotifications = () => {
    firebase.notifications().removeAllDeliveredNotifications();
  };
}
