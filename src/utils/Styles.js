import {StyleSheet} from 'react-native';
import Colors from './Colors';

export default Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.screen_bg_color,
  },
  floatingActionButton: {
    width: 60,
    height: 60,
    backgroundColor: Colors.primary_dark,
    position: 'absolute',
    borderRadius: 30,
    bottom: 10,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    shadowColor: 'black',
  },
  dataDropDownView: {
    flexDirection: 'row',
    borderRadius: 5,
    borderColor: 'black',
    borderWidth: 0.5,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 10,
    margin: 10,
  },
  dataDropDown: {
    flex: 1,
  },
  dataDropDownText: {
    fontSize: 16,
  },
  dataDropDownIcon: {
    marginRight: 5,
    alignSelf: 'center',
  },
  dataDropDownItemText: {
    fontSize: 16,
    color: 'gray',
  },
  whiteContainer: {
    flex: 1,
    backgroundColor: 'white',
    padding: 16,
  },
  normalContainer: {
    flex: 1,
    backgroundColor: Colors.screen_bg_color,
    padding: 10,
  },
  logoHeaderStyle: {
    backgroundColor: 'white',
    elevation: 0,
    height: 100,
  },
  rowDirectionView: {
    flexDirection: 'row',
  },
  toolbarIconView: {
    marginLeft: 5,
    marginRight: 5,
  },
  toolbarCountView: {
    position: 'absolute',
    top: 0,
    right: -5,
    width: 15,
    height: 15,
    backgroundColor: Colors.accent,
    borderRadius: 15 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  toolbarCountText: {
    color: 'white',
    fontSize: 12,
  },
  divider: {
    borderBottomColor: 'darkgray',
    borderBottomWidth: 1,
  },
  commonButton: {
    color: Colors.buttonText,
    alignSelf: 'center',
    backgroundColor: Colors.primary_dark,
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
    paddingLeft: 20,
    borderRadius: 10,
    marginTop: 10,
  },
  fullFlex: {
    flex: 1,
  },
  fullButton: {
    color: Colors.buttonText,
    backgroundColor: Colors.primary_dark,
    width: '100%',
    fontSize: 16,
    borderRadius: 10,
    marginTop: 10,
    textAlign: 'center',
    padding: 10,
  },
});
