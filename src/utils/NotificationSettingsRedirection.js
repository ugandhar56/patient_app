import { AsyncStorage, Alert } from "react-native";
import AndroidOpenSettings from "react-native-android-open-settings";
import { getManufacturer } from "react-native-device-info";
import En from "./En";

export default class NotificationSettingsRedirection {
  checkNotificationAlertsShown() {
    AsyncStorage.getItem("isNotificationBackgroundEnableDialogShown").then(
      (value) => {
        if (value == null) {
          getManufacturer().then((manufacturer) => {
            if (
              manufacturer.toLowerCase() == "Xiaomi".toLowerCase() ||
              manufacturer.toLowerCase() == "oppo".toLowerCase() ||
              manufacturer.toLowerCase() == "Honor".toLowerCase() ||
              manufacturer.toLowerCase() == "Huawei".toLowerCase() ||
              manufacturer.toLowerCase() == "vivo".toLowerCase()
            ) {
              this.showXiaomiNotificationAlert();
            } else if (manufacturer.toLowerCase() == "Lenovo".toLowerCase()) {
              this.showLenovoNotificationAlert();
            }
          });
        }
      }
    );
  }

  showXiaomiNotificationAlert() {
    Alert.alert(
      En.enable_xiaomi_notification_title,
      En.enable_xiaomi_notification_msg,
      [
        {
          text: En.yes,
          onPress: this.redirectToNotificationSettingsScreen,
          style: "cancel",
        },
        { text: En.skip, onPress: this.skipNotificationsSettingsAlert },
      ],
      { cancelable: false }
    );
  }

  showLenovoNotificationAlert() {
    Alert.alert(
      En.background_app_refresh_title,
      En.background_app_refresh_msg,
      [
        {
          text: En.yes,
          onPress: this.redirectToSettingsScreen,
          style: "cancel",
        },
        { text: En.skip, onPress: this.skipNotificationsSettingsAlert },
      ],
      { cancelable: false }
    );
  }

  skipNotificationsSettingsAlert = () => {
    AsyncStorage.setItem("isNotificationBackgroundEnableDialogShown", "false");
  };

  redirectToNotificationSettingsScreen = () => {
    AsyncStorage.setItem("isNotificationBackgroundEnableDialogShown", "true");
    AndroidOpenSettings.appNotificationSettings();
  };

  redirectToSettingsScreen = () => {
    AsyncStorage.setItem("isNotificationBackgroundEnableDialogShown", "true");
    AndroidOpenSettings.generalSettings();
  };
}
