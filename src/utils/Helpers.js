import moment from "moment";
import { AsyncStorage } from "react-native";
import { Actions } from "react-native-router-flux";
import SocketManager from "../components/SocketManager";
// import { RtcEngine } from "react-native-agora";
// import CookieManager from "@react-native-community/cookies";

export default Helpers = {
  getDateTimeFromSeconds: function (time, format) {
    return moment(time * 1000).format(format);
  },
  validateEmail: function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  },
  validateSpaceInPassword: function (password) {
    var spaceRegex = /^.+\s.+$/g;
    if (password.match(spaceRegex)) {
      return false;
    } else {
      return true;
    }
  },
  removeUnderscoreFromString: function (value) {
    return value.split("_").join(" ");
  },
  compareDates: function (startDate, endDate, format) {
    let sd = moment(startDate, format, true).format();
    let ed = moment(endDate, format, true).format();
    return moment(ed).isSameOrAfter(sd);
  },
  // redirectToLogin: function () {
  //   AsyncStorage.getItem("rememberme")
  //     .then((value) => {
  //       let keys;
  //       if (value == "false") {
  //         keys = [
  //           "sessionId",
  //           "userName",
  //           "firstName",
  //           "lastName",
  //           "id",
  //           "password",
  //           "mobileNo",
  //         ];
  //       } else if (value == "true") {
  //         keys = ["sessionId", "firstName", "lastName", "id", "mobileNo"];
  //       }
  //       AsyncStorage.multiRemove(keys, (_) => {
  //         RtcEngine.leaveChannel();
  //         RtcEngine.destroy();
  //         new SocketManager().sendUserOffline();
  //         global.sessionId = "";
  //         global.customerId = "";
  //         // clear cookies
  //         CookieManager.clearAll().then((res) => {
  //           Actions.push("login");
  //         });
  //       });
  //     })
  //     .done();
  // },
  isInteger: function (s) {
    if (isNaN(s)) {
      return false;
    } else if (s.toString().indexOf(".") != -1) {
      return false;
    } else {
      return true;
    }
  },
  showMaskedMobileNumber: function () {
    var number = "9848937015";
    var partialNumber = number.slice(6, 10);
    return "XXX" + partialNumber;
  },
  istDateTime: function (dateTime) {
    var offset = dateTime.getTimezoneOffset();

    var ISTOffset = 330; // IST offset UTC +5:30

    var ISTTime = new Date(dateTime.getTime() + (ISTOffset + offset) * 60000);

    return ISTTime;
  },
  dateDifference: function (startDate, endDate) {
    var startDate = moment(startDate, "DD-MM-YYYY");
    var endDate = moment(endDate, "DD-MM-YYYY");
    return endDate.diff(startDate, "years");
  },
  showNotAvailableText: function (s) {
    let text = s;
    if (text == null || text == undefined || text.length == 0) {
      text = "NA";
      return text;
    } else {
      return text;
    }
  },
};
