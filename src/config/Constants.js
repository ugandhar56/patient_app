// import { React } from 'react';
import React, { Component } from 'react'

class IConstants extends Component 
{
  MethodTypes = new (function () {
    this.LOGIN = 1;
    this.OTP_REGISTER = 2;
    this.HOME = 3;
    this.ORDER = 4;
    this.ORDER_INFO = 5;
    this.PRODUCTS_LIST = 6;
    this.STOCK_INFO = 7;
    this.GET_ITEM_IMAGE = 8;
    this.GET_MASTER_INFO = 9;
    this.DELIVERY = 10;
    this.ORDER_UPDATE = 11;
  });
}

const clIConstants = new IConstants();
export default clIConstants;
