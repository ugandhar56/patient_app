import React, { Component } from 'react'
import IConstants from "./Constants.js";
import {METHOD_LOGIN,METHOD_OTP_REGISTER} from '../config/serviceURLS';
class Utilities extends Component
{
    getUrl(iMethodType)
    {
        var sMethod;
        if(iMethodType==IConstants.MethodTypes.LOGIN)
            sMethod=METHOD_LOGIN;
        else if(iMethodType==IConstants.IMethodTypes.OTP_REGISTER)
            sMethod=METHOD_OTP_REGISTER;
        // else if(iMethodType==IConstants.IMethodTypes.HOME)
        //     sMethod=METHOD_HOME;
        // else if(iMethodType==IConstants.IMethodTypes.ORDER)
        //     sMethod=METHOD_ORDER;
        // else if(iMethodType==IConstants.IMethodTypes.ORDER_INFO)
        //     sMethod=METHOD_ORDER_INFO;
        // else if(iMethodType==IConstants.IMethodTypes.PRODUCTS_LIST)
        //     sMethod=METHOD_PRODUCTS_LIST;    
        // else if(iMethodType==IConstants.IMethodTypes.STOCK_INFO)
        //     sMethod=METHOD_GET_STOCK_INFO;   
        // else if(iMethodType==IConstants.IMethodTypes.GET_ITEM_IMAGE)
        //     sMethod=METHOD_GET_ITEMIMAGE;
        // else if(iMethodType==IConstants.IMethodTypes.GET_MASTER_INFO)
        //     sMethod=METHOD_MASTER_INFO;
        // else if(iMethodType==IConstants.IMethodTypes.DELIVERY)
        //     sMethod=METHOD_DELIVERS;
        // else if(iMethodType==IConstants.IMethodTypes.ORDER_UPDATE)
        //     sMethod=METHOD_UPDATE_ORDER_STATUS;    

        return "/"+sMethod;
    }
}
const clUtilities=new Utilities();
export default clUtilities;